##############################
# Define start and end date
#############################

start <- as.Date("2020-01-22")
end <- as.Date(Sys.Date())


cryptos = paste0(c("BTC","ETH","XRP",
                   "USDT","BCH","LTC",
                   "EOS","BNB","XLM",
                   "LINK","TRX","ADA",
                   "XMR","ETC","DASH",
                   "NEO","MIOTA","XEM",
                   "ZEC","DOGE","BAT",
                   "VET"),"-EUR")


for (i in 1:length(cryptos)) {
  getSymbols(cryptos[i], src = "yahoo", from = start, to = end)
}



xtstodf <- function(xts_obj) {
  
  df =  data.frame(date=index(xts_obj), coredata(xts_obj))
  return(df)

}

list2env(lapply(Filter(is.xts, as.list(.GlobalEnv)), xtstodf), envir = .GlobalEnv)


clean_cols <- function(df) {
  
  df$crypto = word(colnames(df)[2],1,sep = "\\.")
  
  colnames(df) = c("date","open","high",
                          "low","close","volume",
                          "adjusted","crypto")
  
  return(df)
  
}


list2env(lapply(Filter(is.data.frame, as.list(.GlobalEnv)), clean_cols), envir = .GlobalEnv)

dflist <- Filter(is.data.frame, as.list(.GlobalEnv))

crypto_data<- rbindlist(dflist)

crypto_data$updated_at = max(crypto_data$date)

crypto_data$script_runtime <- Sys.time()

filename = paste0("datasets/", "crypto_data.csv")

write.csv(crypto_data,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())




