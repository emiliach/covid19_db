
BASE_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/'
CONFIRMED = 'time_series_19-covid-Confirmed.csv'
DEATH = 'time_series_19-covid-Deaths.csv'
RECOVERED = 'time_series_19-covid-Recovered.csv'



confirmed_cases <-read_csv(url(paste(BASE_URL,CONFIRMED,sep = '')))

death_cases <- read_csv(url(paste(BASE_URL,DEATH,sep = '')))

recovered_cases <-read_csv(url(paste(BASE_URL,RECOVERED,sep = '')))

###########################
# Data preprocessing
##########################

################################# 
# Change data from wide to long
#################################


melt_fun <- function(df) {
  
  df = reshape2::melt(df, id.vars=c("Province/State", "Country/Region","Lat","Long"))
  return(df)
  
}

# apply one function to all existing dataframes at once

#dflist <- Filter(is.data.frame, as.list(.GlobalEnv))
list2env(lapply(Filter(is.data.frame, as.list(.GlobalEnv)), melt_fun), envir = .GlobalEnv)


###############################
# Change column names
##############################

colnames(confirmed_cases) = c("State","Country","Lat","Long","Date","ConfirmedCases")

colnames(death_cases) = c("State","Country","Lat","Long","Date","Deaths")

colnames(recovered_cases) = c("State","Country","Lat","Long","Date","RecoveredCases")


################################
## Merge dataframes
################################

covid19 <- merge(confirmed_cases,death_cases,by=c("State","Country","Lat","Long","Date"))
covid19 <- merge(covid19,recovered_cases,by=c("State","Country","Lat","Long","Date"))
covid19$Date<- lubridate::parse_date_time(covid19$Date, orders = c("ymd", "dmy", "mdy"))

covid19$updated_at <- max(covid19$Date)

covid19$script_runtime= Sys.time()

filename = paste0("datasets/", "covid19.csv")

write.csv(covid19,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())





