##################
# Set base URL
##################

BASE_URL = 'https://de.finance.yahoo.com/industries/'


##########################################################
# Set all the sectors available in yahoo finance for DE
##########################################################

cohorts = c("energy","financial","healthcare",
            "business_services","telecom_utilities",
            "hardware_electronics","software_services","manufacturing_materials",
            "consumer_products_media","industrials","diversified_business",
            "retailing_hospitality")


##########################
# read html,parse table 
##########################

get_stock_names <- function(url) {

  webpage <- readLines(url)
  html <- htmlTreeParse(webpage, useInternalNodes = TRUE, asText = TRUE)
  tableNodes <- getNodeSet(html, "//table")
  symbol <- readHTMLTable(tableNodes[[1]])
  return(symbol)
}

#############################################
# create an empty df with name of sectors
#############################################

df = setNames(replicate(length(cohorts),data.frame()),cohorts)

for (i in 1:length(cohorts)) {
df[i]<-  get_stock_names(paste0(BASE_URL,cohorts[i],sep=''))
}



## Create empty 
stock_name<- NULL

###########################################
# fill the values from df to stock_name
##########################################

for (i in cohorts) {
  stock_name<- c(stock_name,paste(levels(df[[i]]),i,sep = ' '))
}

stock_name = as.data.frame(stock_name)

stock_name = stock_name  %>% separate(stock_name,c("stock_name","sector"),sep = " ")

write.csv(stock_name,"datasets/StockNames.csv",row.names = F)

rm(list = ls())
