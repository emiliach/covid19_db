##############################
# Define start and end date
#############################

start <- as.Date("2020-01-22")
end <- as.Date(Sys.Date())


stocks = paste0(c("BAYN","FME",
                   "EOAN","LHA","LIN",
                   "RWE","ADS","DPW",
                   "VOW3","HEN3","1COV",
                   "DBK","VNA","SAP",
                   "BAS","BMW","DTE",
                   "CON","SIE","IFX",
                   "MUV2","MRK","DB1",
                  "DAI","TKA","FRE",
                  "ALV","WDI"),".DE")

stocknames = read.csv("datasets/StockNames.csv")
stocks = c(stocks,levels(stocknames$stock_name))
rm(stocknames)

for (i in 1:length(stocks)) {
  tryCatch({
  getSymbols(stocks[i], src = "yahoo", from = start, to = end)
  }, error=function(e){})
}



xtstodf <- function(xts_obj) {
  
  df =  data.frame(date=index(xts_obj), coredata(xts_obj))
  return(df)
  
}

list2env(lapply(Filter(is.xts, as.list(.GlobalEnv)), xtstodf), envir = .GlobalEnv)


clean_cols <- function(df) {
  
  df$stock = word(colnames(df)[2],1,sep = "\\.")
  
  colnames(df) = c("date","open","high",
                   "low","close","volume",
                   "adjusted","stocks")
  
  return(df)
  
}


list2env(lapply(Filter(is.data.frame, as.list(.GlobalEnv)), clean_cols), envir = .GlobalEnv)

dflist <- Filter(is.data.frame, as.list(.GlobalEnv))

stocks_data<- rbindlist(dflist)

stocks_data$updated_at = max(stocks_data$date)

stocks_data$script_runtime <- Sys.time()

filename = paste0("datasets/", "stocks_data.csv")

write.csv(stocks_data,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())

