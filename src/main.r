
################################# 
# Load all important libraries 
#################################

install_and_load <- function(packages) {
  k <- packages[!(packages %in% installed.packages()[, "Package"])];
  if (length(k)) { install.packages(k, repos = 'https://cran.rstudio.com/'); }
  
  for (package_name in packages) { library(package_name, character.only = TRUE, quietly = TRUE); }
}

install_and_load(c("here", "readr", "reshape2","dplyr","lubridate",
                   "quantmod","data.table","stringr"))



path = here::here()

setwd(path)


############################
# source all the scripts
##########################

source("src/generate_cases_csv.r")
source("src/get_crypto_data.r")
source("src/get_stocks_names.r")
source("src/get_stocks_data.r")
